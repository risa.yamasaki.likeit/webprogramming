package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      Date birthDate = rs.getDate("birth_date");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, _password, birthDate, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public User findById(int uId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id = ? ";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, uId);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      String password = rs.getString("password");
      Date birthDate = rs.getDate("birth_date");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, password, birthDate, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

  }

  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        String password = rs.getString("password");
        Date birthDate = rs.getDate("birth_date");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, password, birthDate, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }



  public void insert(String loginId, String name, String password, Date birthday) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO user  (login_id, name, password, birth_date,create_date,update_date )"
              + "VALUES(?,?,?,?,now(),now())";


      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, password);
      pStmt.setDate(4, birthday);
      pStmt.executeUpdate();
      /*
       * if (!rs.next()) {
       * 
       * } int id = rs.getInt("id"); String _loginId = rs.getString("login_id"); String _name =
       * rs.getString("name"); String _password = rs.getString("password"); Date birthDate =
       * rs.getDate("birth_date"); boolean isAdmin = rs.getBoolean("is_admin"); Timestamp createDate
       * = rs.getTimestamp("create_date"); Timestamp updateDate = rs.getTimestamp("update_date");
       */

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  public void update(int id, String name, String password, Date birthday) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "UPDATE user SET  name = ?, password = ?, birth_date = ? where id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, password);
      pStmt.setDate(3, birthday);
      pStmt.setInt(4, id);
      pStmt.executeUpdate();

      // if (!rs.next()) {

      // }
      // int _id = rs.getInt("id");
      // String _loginId = rs.getString("login_id");
      // String _name = rs.getString("name");
      // String _password = rs.getString("password");
      // Date birthDate = rs.getDate("birth_date");
      // boolean isAdmin = rs.getBoolean("is_admin");
      // Timestamp createDate = rs.getTimestamp("create_date");
      // Timestamp updateDate = rs.getTimestamp("update_date");


    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  public void delete(int id) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "DELETE FROM user WHERE id = ?";
      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      pStmt.executeUpdate();




    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  public List<User> search(String loginId, String name, String dateStart, String dateEnd) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();
    List<String> sqlParameterList = new ArrayList<>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE is_admin = false";
      StringBuilder stringBuilder = new StringBuilder(sql);


      if (!loginId.equals("")) {
        stringBuilder.append(" and login_id = ?");
        sqlParameterList.add(loginId);
      }

      if (!name.equals("")) {
        stringBuilder.append(" and name like ?");
        sqlParameterList.add("%" + name + "%");
      }

      if (!dateStart.equals("")) {
        stringBuilder.append(" and birth_date >= ?");
        sqlParameterList.add(dateStart);
      }
      
      if (!dateEnd.equals("")) {
        stringBuilder.append(" and birth_date <= ?");
        sqlParameterList.add(dateEnd);
      }
      
      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
      for (int i = 0; i < sqlParameterList.size(); i++) {
        pStmt.setString(i + 1, sqlParameterList.get(i));
      }

      ResultSet rs = pStmt.executeQuery();
      
      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id");
        String _name = rs.getString("name");
        String password = rs.getString("password");
        Date birthDate = rs.getDate("birth_date");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, _loginId, _name, password, birthDate, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

}
