package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      // ユーザー情報取得
      HttpSession session = request.getSession();
      User userInfo = (User) session.getAttribute("userInfo");

      // ユーザー情報取得できない場合
      if (userInfo == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // ユーザー情報取得できたら、 userAdd.jspにフォワード
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userAdd.jsp");
      dispatcher.forward(request, response);

      // ログインサーブレットにリダイレクト(ログアウト)
      response.sendRedirect("LoginServlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      try {

      // リクエストパラメータ取得
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");

      java.sql.Date date = null;

      // dateが数値のとき型変換
      if (!birthDate.equals("")) {
        date = java.sql.Date.valueOf(birthDate);
      }


      // リダイレクト
      // response.sendRedirect("UserList");
      

      if (!password.equals(passwordConfirm) || loginId.equals("") || date == null
          || name.equals("")) {

        // 入力画面戻る
        User user = new User();
        user.setLoginId(loginId);
        user.setName(name);
        user.setBirthDate(date);
        
        request.setAttribute("user", user);

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません。");


        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userAdd.jsp");
        dispatcher.forward(request, response);
        return;

      }
      String encordstr = PasswordEncorder.encordPassword(password);

      UserDao dao = new UserDao();
      dao.insert(loginId, name, encordstr, date);

        // 成功した時一覧画面に遷移
      response.sendRedirect("UserListServlet");

    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }
}
