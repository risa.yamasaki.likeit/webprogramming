package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {


      try {
      // ユーザー情報取得
      HttpSession session = request.getSession();
      User userInfo = (User) session.getAttribute("userInfo");

      // ユーザー情報取得できない場合
      if (userInfo == null) {
        response.sendRedirect("LoginServlet");
        return;
      }


      String id = request.getParameter("id");
      int num = Integer.valueOf(id).intValue();

      UserDao userDao = new UserDao();
      User user = userDao.findById(num);

      request.setAttribute("user", user);

      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/MockSample/userDelete.jsp");
      dispatcher.forward(request, response);

      // ログインサーブレットにリダイレクト(ログアウト)
      // response.sendRedirect("LoginServlet");
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータ取得
      String id = request.getParameter("user-id");
      int num = Integer.valueOf(id).intValue();

      UserDao userDao = new UserDao();
      userDao.delete(num);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");



  }
}
