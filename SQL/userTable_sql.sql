create database usermanagement default character set utf8;

use usermanagement;

CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);

INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date)
VALUES('admin','�Ǘ���','20050525','password',true,now(),now()); 

use usermanagement;

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)
VALUES('user01','���1', '2001-08-28','password',now(), now());
INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)
VALUES('user02','���2', '2003-09-17','password',now(), now());
INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)
VALUES('user03','���3', '2007-11-30','password',now(), now());

